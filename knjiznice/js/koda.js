
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

var pacienti=[];
var ehrId;
var mapa;
/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke() {
  $("#gen").remove();
  setTimeout(dodajPacienta("Janez", "Janezovic", "1918-12-12"), 10);
  setTimeout(dodajPacienta("Miha", "Lenzky", "1989-11-11"), 10);
  setTimeout(dodajPacienta("Adolf", "Mussolini", "1939-09-01"), 1000);
  console.log(pacienti.toString());
  dodajMeritve(pacienti[pacienti.length-3], "2018-3-19T13:20Z", 39.0, 90, 60, 150, 70, "Sestra Jackie");
  dodajMeritve(pacienti[pacienti.length-3], "2018-4-19T14:10Z", 37.1, 95, 62, 150, 80, "Sestra Jackie");
  dodajMeritve(pacienti[pacienti.length-3], "2018-4-20T12:20Z", 32.1, 0, 0, 150, 120, "Nekdo");
  dodajMeritve(pacienti[pacienti.length-2], "2019-4-19T14:10Z", 36.1, 110, 90, 180, 70, "Sestra Madona");
  dodajMeritve(pacienti[pacienti.length-2], "2019-4-20T12:20Z", 36.1, 115, 85, 180, 75, "Dohtar Nenavadni");
  dodajMeritve(pacienti[pacienti.length-1], "2019-1-20T12:10Z", 36.4, 190, 100, 170, 210, "Dohtar Nenavadni");
}

function dodajMeritve(ehrId, time, temp, kri_tlak_sys, kri_tlak_dia, visina, teza, dude="sam"){
  $.ajaxSetup({
    headers: {
      "Authorization": getAuthorization()
    }
  });
  var compositionData = {
    "ctx/time": time,
    "ctx/language": "en",
    "ctx/territory": "SI",
    "vital_signs/body_temperature/any_event/temperature|magnitude": temp,
    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
    "vital_signs/blood_pressure/any_event/systolic": kri_tlak_sys,
    "vital_signs/blood_pressure/any_event/diastolic": kri_tlak_dia,
    "vital_signs/height_length/any_event/body_height_length": visina,
    "vital_signs/body_weight/any_event/body_weight": teza
  };
  var queryParams = {
    "ehrId": ehrId,
    templateId: 'Vital Signs',
    format: 'FLAT',
    committer: dude
  };
  $.ajax({
    url: baseUrl + "/composition?" + $.param(queryParams),
    type: 'POST',
    contentType: 'application/json',
    data: JSON.stringify(compositionData),
    success: function (res) {
        $("#header").html("Store composition");
        $("#result").html(res.meta.href);
    }
  });
}

function dodajPacienta(ime, priimek, rd){
	if (!ime || !priimek || !rd || ime.trim().length == 0 || priimek.trim().length == 0 || rd.trim().length == 0) {
	  return false;
	} else {
	  var id=0;
		$.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        ehrId = data.ehrId;
        var partyData = {
          firstNames: ime,
          lastNames: priimek,
          dateOfBirth: rd,
          additionalInfo: {"ehrId": ehrId}
        };
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) {
            $("#start_menu").append("<li class=\"start_line\" value=\""+ehrId+"\">"+ime+" "+priimek+"</li>");
            pacienti.push(ehrId);
          }
        });
      }
		});
	}
}

function spremeni(vrsta){
  switch(vrsta){
    case "start":
      $("#start").css("display", "inline-block");
      $("#pacient").css("display", "none");
      $("#dodaj").css("display", "none");
      break;
    case "pacient":
      $("#start").css("display", "none");
      $("#pacient").css("display", "inline-block");
      $("#dodaj").css("display", "none");
      break;
    case "dodaj":
      $("#start").css("display", "none");
      $("#pacient").css("display", "none");
      $("#dodaj").css("display", "inline-block");
      break;
    default:
      $("#start").css("display", "inline-block");
      $("#pacient").css("display", "none");
      $("#dodaj").css("display", "none");
      break;
  }
}

function dodajanjeSub(){
  if(dodajPacienta($("#n_ime").val(), $("#n_priimek").val(), $("#n_rd").val())==false) return 1;
  else spremeni("start");
}

$(document).ready(function(){
  $(".start_line").click(function(){
    spremeni("pacient");
  });
  
  
  var mapOptions = {
    center: [46.05004, 14.46931],
    zoom: 12
  };
  var mapdata;
  mapa = new L.map('mapa_id', mapOptions);
  var layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
  mapa.addLayer(layer);
  jQuery.getJSON("https://teaching.lavbic.net/cdn/OIS/DN3/bolnisnice.json", function(data){
    L.geoJSON(data, {
      onEachFeature: function (feature, layer) {
        var ime=feature.properties.name;
        var ulica=feature.properties["addr:street"];
        var mesto=feature.properties["addr:city"];
        if(ime=="undefined") ime="nedefinirano";
        if(ulica=="undefined") ulica="nedefinirano";
        if(mesto=="undefined") mesto="nedefinirano";
        layer.bindPopup("<h4>"+ime+"</h4><br><h5>"+mesto+", "+ulica+"</h5>");
     }
    }).addTo(mapa);
  });
});
